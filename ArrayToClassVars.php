<?php

/**
 *	kmwatson c2014
 *
 *	This class turns an array into an object with
 *	a class property for each key in the array (set
 *	to the value of that array element).
 *
 *	@input - $properties		A one dimensional array
 *
 *	todo Research why introspection does not work properly
 *			with dynamically definied properties
 *
 */

  class ArrayToClassVars {

	//array of class properties
	public $_data = [];

	//constructor
    public function __construct(Array $properties=array()) {
      $this->_data = $properties;
    }

  /**
     *
     * implement php magic methods
     * __set, __get, and __toString
	 *
     */
    public function __set($property, $value) {
      return $this->_data[$property] = $value;
    }

    public function __get($property) {
      return array_key_exists($property, $this->_data)
        ? $this->_data[$property]
        : null
      ;
    }
        
    public function __toString() {
	    $mystring = '';
	    foreach ($this->_data as $key => $val) {
		    $mystring .= $key . '' . $val. PHP_EOL;
	    }
	    return $mystring;
	    
    }
}  //end class ArraytoClassVars.php
