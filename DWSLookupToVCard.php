<?php

/**
 *	kmwatson c2014
 *
 * A web page that will take JSON input from the UCSF 
 * directory web service and display all of the fields and data.
 *
 * Once the page displays the directory data, a button will allow
 *  the user to create vCard file for download to their local computer.
 *
 * @input - $url		url to JSON data (acquired via $_POST)
 *
 */
include_once 'ArrayToClassVars.php';
include 'JSONUserData.php';
	
// "main" method  
	// check to see if the download button was clicked
	if(isset($_POST['download'])) {
		
		download($_POST['vcardinfo']);
		
	} else {
		
		//init JSON
		$urlContents = file_get_contents(($_POST['url']));
		$jSONData = new JSONUserData($urlContents);	
		
		//init vCard
		$vCardData = new ArrayToClassVars([]);
		populateVCard($jSONData,$vCardData);
	
		//show the JSON info 
		$jSONData->JSONAsHTMLTable();

		//build the download button passing the vCard string
		echo '<form method="post" action="DWSLookupToVCard.php">';
		echo '<p><input type="hidden" name="vcardinfo" value="';
		echo  $vCardData->__toString();
		echo '" size="100"> ';
		echo '  <input type="submit" name="download" value="Download">';
		echo '</form>';	
	}  
	
// end main		

  /**
     * Load the JSON into vCard format
     * 
     * @input - $jSONData		An initialized JSONUserData object
	 *             - $vCardData	An empty ArrayToClassVars object
	 *
     */
	function populateVCard($jSONData, $vCardData) {
		$fieldMappingArray = parse_ini_file('JsonToVCardMap.ini');

		foreach(($fieldMappingArray) as $property => $value)  { 
			//set the vCard data replacing any inline \n & \r with ', '
			$propertyVal= (preg_replace('~[\r\n]+~', ';', (
				     										$jSONData->__get($property))));
			
			$vCardData->__set($value,$propertyVal);
		}
	}

	/**
     * Output the vCard and download to user
     * 
     * @input - $vCardString		string output of vCard object, 
     *                                         (passed from initial load view via $_POST)
	 *
     */
	function download($vCardString) {
		
		$vCardOutput  = "BEGIN:VCARD" . PHP_EOL;
    	$vCardOutput .= "VERSION:3.0" . PHP_EOL;
		$vCardOutput .=$vCardString . PHP_EOL;   		
    	$vCardOutput .= "END:VCARD" . PHP_EOL;
 	
  		//set up & initiate download	
  	  	header("Pragma: public"); 
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false); // required for certain browsers 
  		header("Content-type: text/vcard");
  		header("Content-Disposition: attachment; filename=vCard.vcf");
 		print_r($vCardOutput);
	}  

  	


