<?php

/**
 *	kmwatson c2014
 *
 *	The ArrayToClassVars class turns an array into an object with
 *	a class property for each key in the array (set
 *	to the value of that array element).
 *
 * This class overrides the __get method and adds the
 * JSONAsHTMLTable method for displaying the data
 *
 *	@input - $jsonObject		A json_decode'd array
 *
 * Uses stylesheet JSONUser.css
 *          display name mapping JSONDisplayNames.ini
 *
 */

class JSONUserData extends ArrayToClassVars {

	public $displayNameMappingArray;
    private $styles = 'JSONUser.css';
    private $displayNameFile = 'JSONDisplayNames.ini';
    
    // constructor
    public function __construct($jsonObject) {
	    
	    $cleanResult;
 		$this->displayNameMappingArray = parse_ini_file($this->displayNameFile);
		$jsonObject = json_decode($jsonObject, true);

		// todo Strip off the outer arrays more elegantly
		foreach ($jsonObject as $jsonData) {
		    $cleanResult = $jsonData;
		}		
		$this->_data = $cleanResult[0];
	}
	
	/**
	 * Create a nice display using a stylesheet
	 *
	 */
	public function JSONAsHTMLTable() {
		
		$jsonIterator = new RecursiveIteratorIterator(
    			new RecursiveArrayIterator($this->_data),
    			RecursiveIteratorIterator::SELF_FIRST);
    			
		echo '<!DOCTYPE html>';
		echo '<html>';
		echo '<head>';
		echo '<title>PHP Demo</title>';
		echo '<link href="' . $this->styles . '" rel="stylesheet">';
		echo '</head>';
		echo '<body>  ';  			
		echo '<table id=JSONUser>';	
		
		/**
		 * iterate over the json replacing the titles as 
		  *per JSONDisplayNames.ini
		  */
		foreach ($jsonIterator as $key => $val) {			
    		if(is_array($val)) {
        		//check for null key
        		if ($key != '0') {
	        		if ($displayName = $this->displayNameMappingArray[$key]) {
        				echo "<tr><th>$displayName</th><td TITLE=$key>";
    				} else {
	    				echo "<tr><td>$key</td><td TITLE=$key>";
					}
    			}
    		} else {
        		echo "$val</td></tr>";
    		}
		}
		echo '</table>';
	}
	
	/**
	/* override __get for array access
	  */
    public function __get($property){
    	return array_key_exists($property, $this->_data)
        	? ($this->_data[$property][0])
        	: null
      		;
    }
    
}  // end class JSONUserData.php
